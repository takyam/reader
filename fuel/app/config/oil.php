<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 * 
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * NOTICE:
 * 
 * If you need to make modifications to the default configuraion, copy
 * this file to your app/config folder, and make them in there.
 *
 * This will allow you to upgrade fuel without losing your custom config.
 */

return array(
	'phpunit' => array(
		'autoload_path' => DOCROOT.'fuel/vendor/autoload.php' ,
	),
);

