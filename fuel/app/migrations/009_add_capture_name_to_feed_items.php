<?php

namespace Fuel\Migrations;

class Add_capture_name_to_feed_items
{
	public function up()
	{
		\DBUtil::add_fields('feed_items', array(
			'capture_name' => array('type' => 'text', 'null' => true),
		));
	}

	public function down()
	{
		\DBUtil::drop_fields('feed_items', array('feed_items'));
	}
}