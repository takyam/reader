<?php

namespace Fuel\Migrations;

class Add_favicon_and_title_to_feeds
{
	public function up()
	{
		\DBUtil::add_fields('feeds', array(
			'title' => array('type' => 'text'),
			'favicon_url' => array('type' => 'text', 'null' => true),
		));
	}

	public function down()
	{
		\DBUtil::drop_fields('feeds', array('title', 'favicon_url'));
	}
}