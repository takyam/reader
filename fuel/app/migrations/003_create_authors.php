<?php

namespace Fuel\Migrations;

class Create_authors
{
	public function up()
	{
		\DBUtil::create_table('authors', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'link' => array('type' => 'text', 'null' => true),
			'email' => array('type' => 'text', 'null' => true),
			'created_at' => array('type' => 'datetime'),
			'updated_at' => array('type' => 'datetime'),
		), array('id'));

		\DBUtil::create_index('authors', 'name');
	}

	public function down()
	{
		\DBUtil::drop_table('authors');
	}
}