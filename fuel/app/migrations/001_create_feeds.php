<?php

namespace Fuel\Migrations;

class Create_feeds
{
	public function up()
	{
		\DBUtil::create_table('feeds', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'url' => array('constraint' => 1000, 'type' => 'varchar'),
			'created_at' => array('type' => 'datetime'),
			'updated_at' => array('type' => 'datetime'),
		), array('id'));

		\DBUtil::create_index('feeds', 'url');
	}

	public function down()
	{
		\DBUtil::drop_table('feeds');
	}
}