<?php

namespace Fuel\Migrations;

class Authors_feed_items
{
	public function up()
	{
		\DBUtil::create_table('authors_feed_items', array(
			'feed_item_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'author_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
		), array('feed_item_id', 'author_id'));

		\DBUtil::add_foreign_key('authors_feed_items', array(
			'key' => 'feed_item_id',
			'reference' => array(
				'table' => 'feed_items',
				'column' => 'id',
			),
			'on_update' => 'NO ACTION',
			'on_delete' => 'NO ACTION',
		));

		\DBUtil::add_foreign_key('authors_feed_items', array(
			'key' => 'author_id',
			'reference' => array(
				'table' => 'authors',
				'column' => 'id',
			),
			'on_update' => 'NO ACTION',
			'on_delete' => 'NO ACTION',
		));
	}

	public function down()
	{
		\DBUtil::drop_table('authors_feed_items');
	}
}