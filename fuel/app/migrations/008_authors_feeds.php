<?php

namespace Fuel\Migrations;

class Authors_feeds
{
	public function up()
	{
		\DBUtil::create_table('authors_feeds', array(
			'feed_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'author_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
		), array('feed_id', 'author_id'));

		\DBUtil::add_foreign_key('authors_feeds', array(
			'key' => 'feed_id',
			'reference' => array(
				'table' => 'feeds',
				'column' => 'id',
			),
			'on_update' => 'NO ACTION',
			'on_delete' => 'NO ACTION',
		));

		\DBUtil::add_foreign_key('authors_feeds', array(
			'key' => 'author_id',
			'reference' => array(
				'table' => 'authors',
				'column' => 'id',
			),
			'on_update' => 'NO ACTION',
			'on_delete' => 'NO ACTION',
		));
	}

	public function down()
	{
		\DBUtil::drop_table('authors_feeds');
	}
}