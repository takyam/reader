<?php

namespace Fuel\Migrations;

class Create_feed_items
{
	public function up()
	{
		\DBUtil::create_table('feed_items', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'feed_id' => array('constraint' => 11, 'type' => 'int'),
			'item_id' => array('constraint' => 1000, 'type' => 'varchar'),
			'link' => array('constraint' => 1000, 'type' => 'varchar'),
			'title' => array('type' => 'text'),
			'description' => array('type' => 'text', 'null' => true),
			'content' => array('type' => 'text', 'null' => true),
			'copyright' => array('type' => 'text', 'null' => true),
			'posted_at' => array('type' => 'datetime'),
			'created_at' => array('type' => 'datetime'),
			'updated_at' => array('type' => 'datetime'),
		), array('id'));

		\DBUtil::create_index('feed_items', 'link');
	}

	public function down()
	{
		\DBUtil::drop_table('feed_items');
	}
}