<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>Takyam Reader</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<?php echo Asset::css(array('bootstrap.min.css', 'bootstrap-responsive.min.css', 'main.css')); ?>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="brand" href="#">TAKYAM READER</a>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12" id="main">
			<?php echo $content; ?>
		</div>
	</div>
	<hr>
	<footer>
		<p>&copy; Company 2013</p>
	</footer>
</div>
<?php echo Asset::js(array('jquery-1.9.1.min.js', 'underscore-min.js', 'backbone-min.js', 'bootstrap.min.js', 'main.js')); ?>
</body>
</html>
