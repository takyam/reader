<?php
class Controller_File extends Controller
{
	public function get_thumbnail($feed_item_id)
	{
		if(!$feed_item = Model_Feed_Item::find($feed_item_id)
			or !isset($feed_item->capture_name)
			or !$feed_item->capture_name){
			throw new HttpNotFoundException();
		}
		$thumbnail_path = APPPATH.'/captures/'.$feed_item->capture_name.'/thumbnail.jpg';
		if(!file_exists($thumbnail_path)){
			throw new HttpNotFoundException();
		}
		return Response::forge(file_get_contents($thumbnail_path), 200, array(
			'Content-Type' => 'image/jpeg'
		));
	}
}