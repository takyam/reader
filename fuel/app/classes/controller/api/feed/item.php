<?php
class Controller_Api_Feed_Item extends Controller_Rest
{
	const MAX_LIMIT = 50;

	public function get_all($offset = 0, $limit = 20)
	{
		$this->response(array_merge(array_map(function($feed_item){ return $feed_item->to_array(); },
			Model_Feed_Item::find('all', array(
				'order_by' => array('id' => 'desc'),
				'offset' => (int)$offset,
				'limit' => max((int)$limit, static::MAX_LIMIT),
			))
		)));

	}
}