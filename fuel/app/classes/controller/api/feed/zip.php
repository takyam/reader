<?php
class Controller_Api_Feed_Zip extends Controller
{
	const MAX_LIMIT = 50;

	public function get_all($offset = 0, $limit = 20)
	{
		$feed_items = Model_Feed_Item::find('all', array(
			'order_by' => array('id' => 'desc'),
			'offset' => (int)$offset,
			'limit' => max((int)$limit, static::MAX_LIMIT),
		));

		$save_dir_path = Config::get('phantomjs.capture.save_dir_path', null);

		//Create Zip Archive
		$file = tempnam('tmp', 'zip');
		$zip = new ZipArchive();
		$res = $zip->open($file, ZipArchive::CREATE);
		if ($res === true) {
			foreach($feed_items as $feed_item){
				if(!isset($feed_item->capture_name) or empty($feed_item->capture_name)){
					continue;
				}
				$zip_path = $save_dir_path . '/' . $feed_item->capture_name . '/' . $feed_item->capture_name . '.zip';
				if(file_exists($zip_path)){
					$zip->addFile($zip_path, $feed_item->capture_name . '.zip');
				}
			}
			$zip->close();
			$response = Response::forge(readfile($file), 200, array(
				'Content-Type' => 'application/zip',
				'Content-Length' => filesize($file),
				'Content-Disposition' => 'attachment; filename="file_items.zip"'
			));
			unlink($file);
			return $response;
		}
	}
}