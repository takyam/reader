<?php
class Controller_Api_Feed extends Controller_Rest
{
	const MAX_LIMIT = 50;

	public function get_all($offset = 0, $limit = 20)
	{
		$this->response(array_merge(array_map(function($feed){ return $feed->to_array(); }, Model_Feed::find('all', array(
			'order_by' => array('id' => 'desc'),
			'offset' => (int)$offset,
			'limit' => max((int)$limit, static::MAX_LIMIT),
		)))));
	}

	public function get_items($feed_id, $offset = 0, $limit = 20)
	{
		$this->response(array_merge(array_map(function($feed_item){ return $feed_item->to_array(); },
			Model_Feed_Item::find('all', array(
				'where' => array('feed_id' => $feed_id),
				'order_by' => array('id' => 'desc'),
				'offset' => (int)$offset,
				'limit' => max((int)$limit, static::MAX_LIMIT),
			))
		)));
	}

	public function post_index()
	{
		try{
			$feed = Model_Feed::get_by_url(Input::json('url', null));
			$feed->authors;
			$this->response($feed->to_array());
		}catch(InvalidArgumentException $error){
			$this->response(array('error' => $error->getMessage()), 409);
		}catch(Exception $error){
			Log::error($error->getMessage());
			throw new HttpServerErrorException();
		}
	}
}