<?php
class Capture
{
	protected static $bin_path = null;
	protected static $script_path = null;
	protected static $save_dir_path = null;

	/**
	 * Configを取得する
	 * @throws FuelException
	 */
	public static function _init()
	{
		if (!static::$bin_path || !static::$script_path || !static::$save_dir_path) {
			$bin_path = Config::get('phantomjs.bin_path', null);
			$save_dir_path = Config::get('phantomjs.capture.save_dir_path', null);
			$script_path = Config::get('phantomjs.capture.script_path', null);
			if (!$bin_path || !$save_dir_path || !$script_path) {
				throw new FuelException('phantomjsの設定を取得dえきませんでした');
			}
			if (!file_exists($bin_path) || !file_exists($script_path) || !is_dir($save_dir_path)) {
				throw new FuelException('phantomjsの設定が間違っています');
			}

			static::$bin_path = $bin_path;
			static::$script_path = $script_path;
			static::$save_dir_path = $save_dir_path;
		}
	}

	/**
	 * @param $url
	 * @param null $name
	 * @return bool|string
	 */
	public static function run($url, $name = null)
	{
		if (is_null($name)) {
			$name = uniqid();
		}
		$name = preg_replace('/\.jpg$/', '', $name);
		$save_path = static::$save_dir_path . '/' . $name;

		try {

			if(!is_dir($save_path)){
				mkdir($save_path, 0777, true);
			}

			$original_jpg_path = $save_path . '/original.jpg';
			$thumbnail_jpg_path = $save_path . '/thumbnail.jpg';

			//Capture web page with phantomjs
			$command = implode(' ', array(static::$bin_path, static::$script_path, $url, $original_jpg_path));
			exec($command, $output, $return);
			if($return !== 0){
				throw new Exception();
			}

			//Resize and crop captured file
			Image::load($original_jpg_path)->crop_resize(300)->save($thumbnail_jpg_path, 0777);

			//Create Zip Archive
			$zip_path = $save_path . '/' . $name . '.zip';
			$zip = new ZipArchive();
			$res = $zip->open($zip_path, ZipArchive::CREATE);
			if($res === true)
			{
				if(file_exists($thumbnail_jpg_path)){
					$zip->addFile($thumbnail_jpg_path, 'thumbnail.jpg');
				}
				$html_path = $save_path . '/original.html';
				if(file_exists($html_path)){
					$zip->addFile($html_path, 'original.html');
				}
				$text_path = $save_path . '/original.txt';
				if(file_exists($text_path)){
					$zip->addFile($text_path, 'original.txt');
				}
				$zip->close();
			}

			return $save_path;
		} catch (Exception $error) {
			return false;
		}
	}
}