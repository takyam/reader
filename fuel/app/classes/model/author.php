<?php

class Model_Author extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'name',
		'link',
		'email',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);
	protected static $_table_name = 'authors';
	protected static $_many_many = array('feed_items', 'feeds');

	protected static $_to_array_exclude = array('created_at');

	/**
	 * SimplePie_Authorを元にモデルを返す
	 * @param SimplePie_Author $simple_pie_author
	 * @param bool $execute_save
	 * @return Model_Author|static
	 */
	public static function get_by_simple_pie_author(SimplePie_Author $simple_pie_author, $execute_save = true)
	{
		//モデルが存在する場合はそのまま返す
		$author = static::find_by_name($simple_pie_author->get_name());
		if ($author and $author instanceof static) {
			return $author;
		}

		//モデルが存在しない場合は生成して返す
		$author = static::forge();
		$author->name = $simple_pie_author->get_name();
		$author->link = $simple_pie_author->get_link();
		$author->email = $simple_pie_author->get_email();

		//保存する
		if ($execute_save) {
			$author->save(false);
		}

		return $author;
	}
}
