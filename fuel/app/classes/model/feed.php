<?php

class Model_Feed extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'url',
		'title',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);
	protected static $_table_name = 'feeds';
	protected static $_has_many = array('feed_items');
	protected static $_many_many = array('authors');

	protected static $_to_array_exclude = array('created_at', 'feed_items');

	protected static $_simple_pie_instances = array();

	/**
	 * @param $url
	 * @return static
	 * @throws InvalidArgumentException
	 */
	public static function get_by_url($url)
	{
		//既に登録されているフィードであればそのまま返す
		$feed = static::find_by_url($url);
		if ($feed) {
			return $feed;
		}

		//URL形式かどうかを返す
		$val = Validation::instance() or $val = Validation::forge();
		if ($val->_empty($val) || !$val->_validation_valid_url($url)) {
			throw new InvalidArgumentException('URLが空か、形式が誤っています');
		}

		//何らかのエラーがあれば返す
		$simple_pie = static::get_simple_pie_by_url($url);
		if ($simple_pie->error()) {
			throw new InvalidArgumentException('RSSを取得できませんでした。URLが間違っている可能性があります');
		}

		//フィードではない場合URLを発見する可能性があり、その場合URLを返す
		if ($urls = static::get_discovered_urls($url) and is_array($urls)) {
			throw new InvalidArgumentException('RSSを取得できませんでした。もしかして以下のURLではありませんか？' . PHP_EOL .
						implode(PHP_EOL, $urls));
		}

		$feed = static::get_by_simple_pie($simple_pie);

		return $feed;
	}

	/**
	 * 指定されたURLのSimplePieのインスタンスを返す
	 * @param $url
	 * @return SimplePie
	 */
	protected static function get_simple_pie_by_url($url)
	{
		$url = trim($url);
		if (array_key_exists($url, static::$_simple_pie_instances)) {
			$simple_pie = static::$_simple_pie_instances[$url];
		} else {
			$simple_pie = new SimplePie();
			$simple_pie->enable_cache(false);
			$simple_pie->set_feed_url($url);
			$simple_pie->init();
		}
		return $simple_pie;
	}

	public function fetch()
	{
		$simple_pie = new SimplePie();
		$simple_pie->enable_cache(false);
		$simple_pie->set_feed_url($this->url);
		$simple_pie->init();
		//アイテム情報をセットする
		$simple_pie_items = $simple_pie->get_items();
		if ($simple_pie_items and is_array($simple_pie_items)) {
			foreach ($simple_pie_items as $simple_pie_item) {
				$this->feed_items[] = Model_Feed_Item::get_by_simple_pie_item($this, $simple_pie_item, false);
			}
		}
		$this->save(null, true);
	}

	/**
	 * 発見したRSSのURLを返す
	 * @param $url
	 * @return array
	 */
	protected static function get_discovered_urls($url)
	{
		$simple_pie = static::get_simple_pie_by_url($url);
		$urls = array();
		if ($discovered_feeds = $simple_pie->get_all_discovered_feeds() and is_array($discovered_feeds)) {
			$urls = array_map(function ($simple_pie_file) {
				return $simple_pie_file->url;
			}, $discovered_feeds);
		}
		return $urls;
	}

	/**
	 * SimplePie からモデルを返す
	 * @param SimplePie $simple_pie
	 * @return static
	 */
	protected static function get_by_simple_pie(SimplePie $simple_pie)
	{

		$feed = static::find_by_url($simple_pie->feed_url);

		//フィードが取得できない場合は新規作成する
		if (!$feed) {
			$feed = static::forge();
			$feed->url = $simple_pie->feed_url;
		}

		$feed->title = $simple_pie->get_title();

		//作成者情報をセットする
		$simple_pie_authors = $simple_pie->get_authors();
		if ($simple_pie_authors and is_array($simple_pie_authors)) {
			foreach ($simple_pie_authors as $simple_pie_author) {
				$author = Model_Author::get_by_simple_pie_author($simple_pie_author, false);
				if ($author and $author instanceof Model_Author) {
					$feed->authors[] = $author;
				}
			}
		}

		//アイテム情報をセットする
		$simple_pie_items = $simple_pie->get_items();
		if ($simple_pie_items and is_array($simple_pie_items)) {
			foreach ($simple_pie_items as $simple_pie_item) {
				$feed->feed_items[] = Model_Feed_Item::get_by_simple_pie_item($feed, $simple_pie_item, false);
			}
		}

		$feed->save(null, true);

		return $feed;
	}

	/**
	 * キャッシュ土オブジェクトを削除する
	 */
	public static function clear_cached_objects()
	{
		static::$_cached_objects = array();
	}
}
