<?php

class Model_Category extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'name',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);
	protected static $_table_name = 'categories';
	protected static $_many_many = array('feed_items');

	/**
	 * SimplePie_Category を元にモデルを返す
	 * @param SimplePie_Category $simple_pie_category
	 * @param bool $execute_save
	 * @return Model_Category|static
	 */
	public static function get_by_simple_pie_category(SimplePie_Category $simple_pie_category, $execute_save = true)
	{
		//モデルが存在する場合はそのまま返す
		$category = static::find_by_name($simple_pie_category->get_label());
		if ($category and $category instanceof static) {
			return $category;
		}

		//モデルが存在しない場合は生成して返す
		$category = static::forge();
		$category->name = $simple_pie_category->get_label();

		//保存する
		if ($execute_save) {
			$category->save(false);
		}

		return $category;
	}
}
