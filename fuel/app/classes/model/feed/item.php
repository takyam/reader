<?php

class Model_Feed_Item extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'feed_id',
		'item_id',
		'link',
		'title',
		'description',
		'content',
		'copyright',
		'posted_at',
		'capture_name',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);
	protected static $_table_name = 'feed_items';
	protected static $_belongs_to = array('feed' => array('cascade_save' => false));
	protected static $_many_many = array('categories', 'authors');

	protected static $_to_array_exclude = array('created_at');

	/**
	 * SimplePie_Itemからモデルを返す
	 * @param Model_Feed $feed 親フィードのモデル
	 * @param SimplePie_Item $simple_pie_item
	 * @param bool $execute_save
	 * @return static
	 */
	public static function get_by_simple_pie_item(Model_Feed $feed, SimplePie_Item $simple_pie_item,
												  $execute_save = false)
	{
		$string_id = $simple_pie_item->get_id();
		$url = $simple_pie_item->get_link();

		//既に追加されているかどうかを確認する
		if (!$feed->is_new()) {
			//IDがある場合は、大量のデータを精査する必要がある可能性があるので、クエリで直接カウントする
			$feed_item = Model_Feed_Item::find('first', array('where' => array(
				'feed_id' => $feed->id,
				'item_id' => $string_id,
				'link' => $url,
			)));
			//モデルが見つかれば返す
			if ($feed_item) {
				return $feed_item;
			}
		} else {
			//IDが無い場合は、念のためループしてアイテムIDとURLが一致するものが無いか確認する
			foreach ($feed->feed_items as $feed_item) {
				//一致するモデルがあれば返す
				if ($feed_item->item_id == $string_id and $feed_item->link == $url) {
					return $feed_item;
				}
			}
		}

		//まだ存在しないフィードアイテムの場合追加する
		$feed_item = Model_Feed_Item::forge();
		$feed_item->item_id = $simple_pie_item->get_id();
		$feed_item->link = $simple_pie_item->get_link();
		$feed_item->title = $simple_pie_item->get_title();
		$feed_item->description = $simple_pie_item->get_description();
		$feed_item->content = $simple_pie_item->get_content();
		$feed_item->copyright = $simple_pie_item->get_copyright();
		$feed_item->posted_at = $simple_pie_item->get_date('Y-m-d H:i:s');

		//カテゴリーデータを関連付ける
		$simple_pie_categories = $simple_pie_item->get_categories();
		if ($simple_pie_categories and is_array($simple_pie_categories)) {
			foreach ($simple_pie_categories as $simple_pie_category) {
				$category = Model_Category::get_by_simple_pie_category($simple_pie_category, false);
				if ($category and $category instanceof Model_Category) {
					$feed_item->categories[] = $category;
				}
			}
		}

		//作成者情報を関連付ける
		$simple_pie_authors = $simple_pie_item->get_authors();
		if ($simple_pie_authors and is_array($simple_pie_authors)) {
			foreach ($simple_pie_authors as $simple_pie_author) {
				$author = Model_Author::get_by_simple_pie_author($simple_pie_author, false);
				if ($author and $author instanceof Model_Author) {
					$feed_item->authors[] = $author;
				}
			}
		}

		//保存する
		if ($execute_save) {
			$feed_item->save(null, true);
		}

		return $feed_item;
	}

	/**
	 * @return $this
	 */
	public function capture()
	{
		if($this->is_new()){
			return $this;
		}

		$saved_path = Capture::run($this->link, $this->id . '_' . md5($this->item_id));
		if(!$saved_path){
			$this->capture_name = 'fail';
		}else{
			$this->capture_name = basename($saved_path);
		}
		$this->save(false);
		return $this;
	}

	/**
	 * キャッシュ土オブジェクトを削除する
	 */
	public static function clear_cached_objects()
	{
		static::$_cached_objects = array();
	}
}