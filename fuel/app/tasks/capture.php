<?php
namespace Fuel\Tasks;
class Capture
{
	public static function run()
	{
		while(true){
			while($feed_item = \Model_Feed_Item::find('first', array(
				'where' => array(
					array('capture_name', null),
				),
				'order_by' => array('id' => 'desc'),
			))){
				\Cli::write($feed_item->id);
				$feed_item->capture();
			}
			\Model_Feed_Item::clear_cached_objects();
			sleep(5);
		}
	}
}