<?php
namespace Fuel\Tasks;
class Feed
{
	public static function run()
	{
		while(true){
			while($feed = \Model_Feed::find('first', array(
				'where' => array(
					array('updated_at', '<=', date('Y-m-d H:i:s', time() - (60 * 60 * 3))),
				),
				'order_by' => array('id' => 'desc'),
			))){
				\Cli::write($feed->id);
				$feed->fetch();
			}
			\Model_Feed::clear_cached_objects();
			sleep(5);
		}
	}
}