###
Backbone.syncをオーバーライドしてエラーを取得できるようにする
###
Backbone._sync = Backbone.sync
Backbone.sync = (method, model, options)=>
  if options?.error? and typeof(options.error) is 'function'
    original_error = options.error
    options.error = (xhr, error, error_code)=>
      if xhr? and !(xhr.response?) and xhr?.getResponseHeader?
        if xhr.getResponseHeader('content-type').match(/^(application\/json|text\/javascript)/)
          try
            if Backbone.$?.parseJSON?
              xhr.response = Backbone.$.parseJSON(xhr.responseText)
            else if JSON?.parse?
              xhr.response = JSON.parse(xhr.responseText)
          catch error
      original_error.apply(original_error, arguments)
  Backbone._sync.apply(Backbone._sync, [method, model, options])

#############################################################################
####### MODELS
#############################################################################
class Model_Feed extends Backbone.Model
  urlRoot: '/api/feed/index'
  defaults:
    id: null
    url: ''
    title: ''
    created_at: null

class Model_Feed_Item extends Backbone.Model
  urlRoot: '/api/feed/item/index'
  defaults:
    id: null
    feed_id: null
    item_id: ''
    link: ''
    title: ''
    description: ''
    content: ''
    copyright: ''
    posted_at: null
    created_at: null
  get_thumbnail_path: =>
    return "/file/thumbnail/#{@get('id')}"

#############################################################################
####### COLLECTIONS
#############################################################################
class Collection_Feeds extends Backbone.Collection
  url: '/api/feed/all'
  model: Model_Feed

class Collection_Feed_Items extends Backbone.Collection
  url: =>
    "/api/feed/items/#{@feed_id}"
  model: Model_Feed_Item
  feed_id: null
  initialize: (options)=>
    @feed_id = options.id


#############################################################################
####### VIEWS
#############################################################################

###
モーダル
###
class View_Modal extends Backbone.View
  el: '''
    <div class="modal hide fade">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3></h3>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <a href="#" class="btn hide-btn" data-dismiss="modal" aria-hidden="true">キャンセル</a>
        <a href="#" class="btn btn-primary submit-btn">OK</a>
      </div>
    </div>
  '''

  events:
    'click .submit-btn': 'clicked_ok'

  $title: null
  $body: null
  $hide_btn: null
  $submit_btn: null

  initialize: =>
    @$title = @$el.find('.modal-header h3')
    @$body = @$el.find('.modal-body')

    @$hide_btn = @$el.find('.hide-btn')
    @$submit_btn = @$el.find('.submit-btn')

    @$el.on 'shown', => @trigger('shown')
    @$el.on 'hidden', => @trigger('hidden')

  show: =>
    @$hide_btn.show()
    @$el.modal('show')

  hide: =>
    @$el.modal('hide')

  clicked_ok: =>
    @trigger('submit')

  set_title: (html)=>
    @$title.html(html)

  set_body: (html)=>
    @$body.html(html)

  show_cancel_btn: =>
    @$hide_btn.show()

  hide_cancel_btn: =>
    @$hide_btn.hide()

###
  フィードの登録フォーム
###
class View_Regist_Feed extends Backbone.View
  el: '''
    <div id="add_feed">
  	  <form class="form-inline">
  		  <input type="text" class="input-xxlarge" id="feed_url" placeholder="Input new RSS's URL">
        <button type="submit" class="btn">Check the RSS</button>
      </form>
    </div>
  '''

  templates:
    modal:
      loading: _.template '''
        <p id="loading" style="text-align:center;" class="muted">
          Loading...<br>
          <span id="checking_url"></span><br>
          <img src="/assets/img/loading.gif">
        </p>
      '''
      success: _.template '''
        <div style="text-align: center;">
          <h4>以下の内容で登録しました</h4>
          <p><strong><%= feed.get('title') %></strong></p>
          <p><%= feed.get('url') %></p>
        </div>
      '''

  events:
    'submit form': 'open_modal'

  modal: null

  $form: null
  $input: null
  $loading: null
  $checking_url: null

  initialize: =>
    @$input = @$el.find('#feed_url')
    @modal = new View_Modal()
    @modal.on('shown', @modal_shown)

  render: =>
    @$el

  open_modal: =>
    @modal.set_body(@templates.modal.loading())
    @$loading = @modal.$el.find('#loading')
    @$checking_url = @modal.$el.find('#checking_url')
    @modal.show()
    return false

  modal_shown: =>
    url = @$input.val()
    url = 'http://' + url if url.match(/^https?:\/\//) is null
    @$checking_url.text(url)
    feed = new Model_Feed()
    feed.save {url: url},
      success: (model, response, options)=>
        @trigger('registered')
        @modal.set_body(@templates.modal.success(feed: model))
        @modal.hide_cancel_btn()
        @modal.off('submit').on('submit', =>
          @modal.hide()
        )
      error: (model, xhr, options)=>
        @modal.hide()
        if xhr?.response?.error?
          alert(xhr.response.error)
        else
          alert('フィードの取得に失敗しました')

###
  フィード一覧の表示
###
class View_Feeds extends Backbone.View
  el: '''
    <div class="row-fluid" id="feeds">
      <div class="span12">
        <table class="table table-bordered table-striped">
          <thead></thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  '''
  events:
    'click .js-feed-link': 'show_feed'

  templates:
    row: _.template '''
      <tr>
        <td>
          <a href="#" data-id="<%= feed.get('id') %>" class="js-feed-link">
            <%= feed.get('title') %>
          </a>
        </td>
        <td>
          <a href="<%= feed.get('url') %>" target="_blank">
            <%= feed.get('url') %>
          </a>
        </td>
      </tr>
    '''

  collection: null

  $tbody: null

  initialize: =>
    @collection = new Collection_Feeds()
    @collection.on('add', @add_row)
    @fetch()

    @$tbody = @$el.find('tbody')

  render: =>
    @$el

  show_feed: (event)=>
    $link = $(event.target)
    $link = $link.parents('a') if event.target.tagName.toLowerCase() isnt 'a'
    id = $link.attr('data-id')
    router.navigate("feed/#{id}", trigger: true)
    return false

  add_row: (feed)=>
    @$tbody.append(@templates.row(feed: feed))

  fetch: =>
    @collection.fetch
      add: true
      remove: false
      merge: true

class View_Feed_Items extends Backbone.View
  el: '''
    <div class="row-fluid">
      <div class="span12">
        <table class="table table-bordered table-striped">
          <thead></thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  '''
  templates:
    row: _.template '''
      <tr>
        <td>
          <%= feed_item.id %>
        </td>
        <td>
          <img src="<%= feed_item.get_thumbnail_path() %>">
        </td>
        <td>
          <%= feed_item.get('title') %>
        </td>
        <td>
          <a href="<%= feed_item.get('link') %>" target="_blank" style="word-break: break-all;word-wrap: break-word;">
            <%= feed_item.get('link') %>
          </a>
        </td>
      </tr>
    '''

  collection: null
  $tbody = null

  initialize: (options)=>
    @$tbody = @$el.find('tbody')

    @collection = new Collection_Feed_Items(id: options.id)
    @collection.on('add', @add_row)
    @fetch()

  render: =>
    @$el

  add_row: (feed_item)=>
    @$tbody.prepend(@templates.row(feed_item: feed_item))

  fetch: =>
    @collection.fetch
      add: true
      remove: false
      merge: true


#############################################################################
####### ROUTER
#############################################################################
class Router extends Backbone.Router
  routes:
    'top': 'top'
    'feed/:id': 'show_feed'
    '': 'top'
  top: =>
    register = new View_Regist_Feed()
    feeds = new View_Feeds()
    register.on('registered', feeds.fetch)
    @reset_all()
    $main.append(register.render()).append(feeds.render())
  show_feed: (id)=>
    @reset_all()
    feed_items = new View_Feed_Items(id: id)
    $main.append(feed_items.render())
  reset_all: =>
    $main.empty()


#############################################################################
####### RUN-MAIN
#############################################################################
$main = null
router = null
(($)=>
  $main = $('#main')
  router = new Router()
  Backbone.history.start({pushState: false})
)(jQuery)